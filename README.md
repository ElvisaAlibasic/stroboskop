# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
((UKAZI GIT))
git clone https://ElvisaAlibasic@bitbucket.org/ElvisaAlibasic/stroboskop.git
git rm nepotrebno.js
git commit -a -m "Priprava potrebnih JavaScript knjižnic"
git push origin master
```

Naloga 6.2.3:

https://bitbucket.org/ElvisaAlibasic/stroboskop/commits/bdc23d9526a8c1c7486fa8474fb181cb774ab7b4
https://bitbucket.org/ElvisaAlibasic/stroboskop/commits/a3fd80dd3b6776c4aab0f4d77bbfdeb7826481a3

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:

https://bitbucket.org/ElvisaAlibasic/stroboskop/commits/8fb5b88c832cb3694a715f5c40b8ba9384bd1443
Naloga 6.3.2:

https://bitbucket.org/ElvisaAlibasic/stroboskop/commits/262f5bc32507ffed014679416f2fce0553d6adb5

Naloga 6.3.3:

https://bitbucket.org/ElvisaAlibasic/stroboskop/commits/467c243983841e04f0ba81041da828e0e3dde881


Naloga 6.3.4:

https://bitbucket.org/ElvisaAlibasic/stroboskop/commits/bb02151f8f974d02b31a94803e12114225a7af97

Naloga 6.3.5:


```
((UKAZI GIT))
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:

https://bitbucket.org/ElvisaAlibasic/stroboskop/commits/1d0ebaebbb89f7d091e65c56f41cc2636d378c0a

Naloga 6.4.2:

https://bitbucket.org/ElvisaAlibasic/stroboskop/commits/b89a6c0fce063acc17605272aeb57cd6379a35b8

Naloga 6.4.3:

https://bitbucket.org/ElvisaAlibasic/stroboskop/commits/d04276761b6dcc28c01ebdf38b0100693e3e5634
Naloga 6.4.4:

https://bitbucket.org/ElvisaAlibasic/stroboskop/commits/419a2dbec26f3925048b6af1f553154bf3904050